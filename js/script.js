// 1. Опишіть своїми словами що таке Document Object Model (DOM)

// Це обєктна модель документа, яка представляє вміст сторінки, з яким можна проводити маніпуляції.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

// innerHTML дозволяє маніпулювати HTML-кодом всередині елемента, а innerText працює тільки з текстом і ігнорує будь-які HTML-теги

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// Є такі методи: querySelector, querySelectorAll, getElementById, getElementsByName, getElementsByTagName, getElementsByClassName. Кращий querySelectorAll.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

// Отримати елементи

// , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let paragraphs = document.getElementsByTagName("p");
for (let i = 0; i < paragraphs.length; i++) {
  paragraphs[i].style.backgroundColor = "#ff0000";
}

let optionsList = document.getElementById("optionsList");
console.log("Елемент з id 'optionsList':", optionsList);
let parentElement = optionsList.parentElement;
console.log("Батьківський елемент:", parentElement);
if (optionsList.hasChildNodes()) {
  let childNodes = optionsList.childNodes;
  console.log("Дочірні ноди:");
  for (let i = 0; i < childNodes.length; i++) {
    let node = childNodes[i];
    console.log("Назва:", node.nodeName, "Тип:", node.nodeType);
  }
} else {
  console.log("Елемент 'optionsList' не має дочірніх нод.");
}

let thisParagraph = document.getElementById('testParagraph');
thisParagraph.textContent = 'This is a paragraph';

const header = document.querySelector('.main-header');
const elements = header.querySelectorAll('*');

elements.forEach((element) => {
  element.classList.add('nav-item');
  console.log(element);
});

const sectionTitles = document.querySelectorAll('.section-title');

sectionTitles.forEach((element) => {
  element.classList.remove('section-title');
});





